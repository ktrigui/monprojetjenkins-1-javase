package com.btk.myapp;

import java.util.logging.Logger; 

/**
 * Hello world!
 *
 */
public class App 
{
	private static Logger logger = Logger.getLogger(App.class.getName());
	
    public static void main( String[] args )
    {
    	
    	for (int i = 1; i <= 10; i++) {
    		logger.info("Ici le programme sous Jenkins - itération : "+i + "/10");
			
		}
    	logger.info( "==== FIN DU PROGRAMME =====" );
    }
}
